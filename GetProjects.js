class GetProjects {
    constructor() {
        this.project_names=[];
        this.total_pages = null;
    }

    collect_project_names_per_page(projects_per_page) {
        for (let i = 0; i < projects_per_page.length; i++) {
            this.project_names.push(projects_per_page[i].name)
        }
    }

    get_projects_list(page=1) {
        cy.request({
            method: 'GET',
            // url could be returned from configs
            // url   : `https://gitlab.com/api/v4/users/Pitrelli/projects?private_token=glpat-QpJThq263z45VpZifHTA&per_page=100&page=${page}`, // includes 1 project
            url   : `https://gitlab.com/api/v4/groups/gitlab-examples/projects?private_token=glpat-QpJThq263z45VpZifHTA&per_page=10&page=${page}`, // includes many projects
            body  : `{}`
        }).then((response) => {
            if (page === 1) { //could be replaced with singleton pattern
                this.total_pages = response.headers["x-total-pages"]
                console.log("total_pages:", this.total_pages)
            }

            this.collect_project_names_per_page(response.body);

            if (page >= this.total_pages) {
                return
            }

            page++;
            console.log("page:", page)

            this.get_projects_list(page); // calling recursion here
        })
    }

}

export const getProjects = new GetProjects();